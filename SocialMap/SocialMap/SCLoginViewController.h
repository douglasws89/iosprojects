//
//  SCLoginViewController.h
//  SocialMap
//
//  Created by Doug on 5/17/13.
//  Copyright (c) 2013 Doug. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SCLoginViewController : UIViewController

- (void)loginFailed;

@end
