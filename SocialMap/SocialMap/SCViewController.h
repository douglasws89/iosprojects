//
//  SCViewController.h
//  SocialMap
//
//  Created by Doug on 5/17/13.
//  Copyright (c) 2013 Doug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

@interface SCViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,CLLocationManagerDelegate>
{
    IBOutlet UISegmentedControl* segmentController;
}

- (IBAction)eventTypeViewChanged:(id)sender;
@property (strong, nonatomic) NSArray *data;
@property (strong, nonatomic) NSArray* array;

@end
