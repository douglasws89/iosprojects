//
//  SCViewController.m
//  SocialMap
//
//  Created by Doug on 5/17/13.
//  Copyright (c) 2013 Doug. All rights reserved.
//

#import "SCViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "SCAppDelegate.h"

@interface SCViewController ()

@property (strong, nonatomic) IBOutlet FBProfilePictureView *userProfileImage;
@property (strong, nonatomic) IBOutlet UILabel *userNameLabel;
@property (strong, nonatomic) IBOutlet UITableView *eventsListTableView;
@property (strong, nonatomic) IBOutlet GMSMapView *gmsMapView;

@end

@implementation SCViewController
{
    NSString* cordLatitude;
    NSString* cordLongitude;
    NSString* title;
    NSString* snippet;
}

@synthesize data = _data;
@synthesize array = _array;

@synthesize gmsMapView = _gmsMapView;
    GMSMapView* mapView_;

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.navigationItem.rightBarButtonItem = [[UIBarButtonItem alloc]
                                              initWithTitle:@"Logout"
                                              style:UIBarButtonItemStyleBordered
                                              target:self
                                              action:@selector(logoutButtonWasPressed:)];
    self.navigationItem.title = @"Social Map";
    
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(sessionStateChanged:)
     name:SCSessionStateChangedNotification
     object:nil];
    

    self.eventsListTableView.hidden = true;
    self.gmsMapView.hidden = true;
    [_gmsMapView removeFromSuperview];
        
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)logoutButtonWasPressed:(id)sender {
    [FBSession.activeSession closeAndClearTokenInformation];
}

- (void)populateUserDetails
{
    if (FBSession.activeSession.isOpen) {
        [[FBRequest requestForMe] startWithCompletionHandler:
         ^(FBRequestConnection *connection,
           NSDictionary<FBGraphUser> *user,
           NSError *error) {
             if (!error) {
                 self.userNameLabel.text = user.name;
                 self.userProfileImage.profileID = user.id;
                 
                 NSString *query =
                 @"{"
                 @"'event_info':'SELECT eid, pic, venue, name, start_time, end_time, creator, host, attending_count from event WHERE eid in (SELECT eid FROM event_member WHERE uid = me())',"
                 @"'event_venue':'SELECT name, location, pic_square, page_id FROM page WHERE page_id IN (SELECT venue.id FROM #event_info)',"
                 @"}";
                 NSDictionary *queryParam = [NSDictionary dictionaryWithObjectsAndKeys:
                                             query, @"q", nil];
                 
                 [FBRequestConnection startWithGraphPath:@"/fql" parameters:queryParam
                                              HTTPMethod:@"GET" completionHandler:^(FBRequestConnection *connection,
                                                                                    id result, NSError *error) {
                                                  if (error) {
                                                      NSLog(@"Error: %@", [error localizedDescription]);
                                                  } else {
                                                      
                                                      NSArray* data = [result objectForKey:@"data"];
                                                        NSArray* events = [((NSDictionary*) data[0]) objectForKey:@"fql_result_set"];
                                                      NSArray* venues = [((NSDictionary*) data[1]) objectForKey:@"fql_result_set"];
                                                      
                                                      NSLog(@"Result: %@", result);
                                                      
                                                      NSArray *friendInfo =
                                                      (NSArray *) [[[result objectForKey:@"data"]
                                                                    objectAtIndex:1]
                                                                   objectForKey:@"fql_result_set"];
                                                      //[self showFriends:friendInfo];
                                                      self.data = venues;
                                                      self.array = events;
                                                      cordLatitude = [[[self.data objectAtIndex:1]
                                                                       objectForKey:@"location"] objectForKey:@"latitude"];
                                                      
                                                      cordLongitude = [[[self.data objectAtIndex:1]objectForKey:@"location"] objectForKey:@"longitude"];
                                                  }
                                              }];

             }
         }];
    }
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    if (FBSession.activeSession.isOpen) {
        [self populateUserDetails];
    }
}

- (void)sessionStateChanged:(NSNotification*)notification {
    [self populateUserDetails];
}

- (IBAction)eventTypeViewChanged:(id)sender {
    NSLog(@"eventViewTypeChanged()...");
        
    // list view
    if (segmentController.selectedSegmentIndex == 0)
    {

        self.eventsListTableView.hidden = false;
        [self.eventsListTableView reloadData];
        self.gmsMapView.hidden = true;      // alpha didnt work
        [_gmsMapView removeFromSuperview];
    }
    
    // map view
    else
    {
        self.eventsListTableView.hidden = true;
        self.gmsMapView.hidden = false;     // alpha didnt work
        [self.gmsMapView reloadInputViews];
    
        [self setMapToEvent];
        
       // GMSMarker *marker = [[GMSMarker alloc] init];
        
        for(int i=0; i <= [self.data count]-1;i++){
            cordLatitude = [[[self.data objectAtIndex:i]
                         objectForKey:@"location"] objectForKey:@"latitude"];
        
            cordLongitude = [[[self.data objectAtIndex:i]objectForKey:@"location"] objectForKey:@"longitude"];
        
            title = [[self.array objectAtIndex:i]
                 objectForKey:@"name"];
            
            snippet = [[[self.data objectAtIndex:i]
                    objectForKey:@"location"] objectForKey:@"street"];
        
            NSMutableArray* arrayAdress = [NSMutableArray arrayWithObjects:cordLatitude, cordLongitude, title, snippet, nil];
        
            for(GMSMarker*marker in arrayAdress)
            {
                GMSMarker *mkr= [[GMSMarker alloc]init];
                [mkr setPosition:CLLocationCoordinate2DMake([cordLatitude doubleValue], [cordLongitude doubleValue])];
                
                [mkr setAnimated:YES];
                [mkr setTitle:title];
                [mkr setSnippet:snippet];
                [mkr setMap:_gmsMapView];
            }
            
        NSLog(@"Add Object!!! %@ %@ %@ %@", cordLatitude, cordLongitude, title, snippet);
        }
    }

}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    NSLog(@"MainViewController:numberOfSectionsInTableView()...");
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSLog(@"called numberofrows: %d",self.data.count);
    
    return [self.data count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    {
        static NSString *CellIdentifier = @"Cell";
        UITableViewCell *cell =
        [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        
        if (cell == nil) {
            cell = [[UITableViewCell alloc]
                    initWithStyle:UITableViewCellStyleDefault
                    reuseIdentifier:CellIdentifier];
        }
        cell.textLabel.text = [[self.array objectAtIndex:indexPath.row]
                                objectForKey:@"name"];//location"] objectForKey:@"street"];
        UIImage *image = [UIImage imageWithData:
                          [NSData dataWithContentsOfURL:
                           [NSURL URLWithString:
                            [[self.array objectAtIndex:indexPath.row]
                             objectForKey:@"pic"]]]];
        cell.imageView.image = image;

        return cell;
    }

}

//ALL MAP STUFF

- (void) setMapToEvent
{
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:[cordLatitude doubleValue]
                                                            longitude:[cordLongitude doubleValue]
                                                                 zoom:10];
    _gmsMapView = [GMSMapView mapWithFrame:CGRectMake(10,158,300,250) camera:camera];
    _gmsMapView.myLocationEnabled = YES;
    
    [self.view addSubview: _gmsMapView];
    
}

- (void) setMapView:(GMSMapView *)mapView
{
    if (!mapView) {
        mapView = [[GMSMapView alloc] initWithFrame:mapView.bounds];
    }
    
    self.gmsMapView = mapView;
}

-(void)findCurrentLocation
{
    
    CLLocationManager *locationManager = [[CLLocationManager alloc] init];
    if ([locationManager locationServicesEnabled])
    {
        locationManager.delegate = self;
        locationManager.desiredAccuracy = kCLLocationAccuracyBest;
        locationManager.distanceFilter = kCLDistanceFilterNone;
        [locationManager startUpdatingLocation];
    }
    
    
    CLLocation *location = [locationManager location];
    CLLocationCoordinate2D coordinate = [location coordinate];
    
    NSString *str=[[NSString alloc] initWithFormat:@" latitude:%f longitude:%f",coordinate.latitude,coordinate.longitude];
    NSLog(@"%@",str);
    
    cordLatitude = [NSString stringWithFormat:@"%f", coordinate.latitude];
    cordLongitude = [NSString stringWithFormat:@"%f", coordinate.longitude];
    
    
}


@end
