//
//  SCAppDelegate.h
//  SocialMap
//
//  Created by Doug on 5/17/13.
//  Copyright (c) 2013 Doug. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>

extern NSString *const SCSessionStateChangedNotification;

@class SCViewController;

@interface SCAppDelegate : UIResponder <UIApplicationDelegate>

- (void)openSession;
- (BOOL)openSessionWithAllowLoginUI:(BOOL)allowLoginUI;
@property (strong, nonatomic) UIWindow *window;

@end
