//
//  main.m
//  SocialMap
//
//  Created by Doug on 5/17/13.
//  Copyright (c) 2013 Doug. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "SCAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([SCAppDelegate class]));
    }
}
